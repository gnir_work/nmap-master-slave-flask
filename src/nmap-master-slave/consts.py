from datetime import timedelta
REST_SERVER_IP = '127.0.0.1'
REST_SERVER_PORT = 5555
MYSQL = {
    'host': '127.0.0.1',
    'user': 'test',
    'password': 'mypass',
    'data_base': 'NmapScans',
    'port': 3306
}

SENDER_MAIL = 'nmaptesting123@gmail.com'
SENDER_PASSWORD = 'nmaptesting'
RECEIVER_MAIL = 'gnir.work@gmail.com'
SMTP_CONFIG = {
    'server': 'smtp.gmail.com',
    'port': 587
}
SCANNING_STATUS = 'scanning'
WAITING_STATUS = 'waiting for worker'
DONE_STATUS = 'done'
FAILED_STATUS = 'failed'
SLAVE_DELAY_TIME = timedelta(seconds=5).total_seconds()
MAX_SCAN_RETRIES = 3
# Please note the when changing the max slave id size change also the generation of the default slave id.
MAX_SLAVE_ID_SIZE = 32
FLASK_DEBUG = False
