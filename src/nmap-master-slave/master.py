import argparse
from collections import namedtuple
import smtplib
import sys

import datetime
from logbook import StreamHandler, Logger, FileHandler
from consts import RECEIVER_MAIL, SENDER_MAIL, SENDER_PASSWORD, SMTP_CONFIG, WAITING_STATUS, \
    FAILED_STATUS, DONE_STATUS, SCANNING_STATUS, REST_SERVER_PORT, MAX_SCAN_RETRIES, FLASK_DEBUG
from orm import NmapScan
from db import get_session
from netaddr import IPNetwork
from flask import Flask, jsonify, request

MASTER_LOG_FILE = 'master_{run_time}.log'.format(run_time=str(datetime.datetime.now()).split('.')[0])
StreamHandler(sys.stdout, bubble=True, level='DEBUG').push_application()
FileHandler(MASTER_LOG_FILE, bubble=True, level='INFO').push_application()
logger = Logger('Master')
session = get_session()
NmapParameters = namedtuple('NmapParameters', ('nmap_params', 'additional_params'))

app = Flask(__name__)
scans = []


def exception_catcher(func):
    """
    Catch and log the exception that were thrown during the excecution of the
    wrapped function.
    """

    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception:
            logger.exception()
            raise

    return wrapper


@app.route('/get_ip_to_scan/<string:slave_id>', methods=["GET"])
def get_ip_to_scan(slave_id):
    """
    An end point through which the slaves ask for a new scan to scan.
    :param slave_id: The id of the slave which asked for the scan.
    :return dict: A dict containing the scan or a message in case there is no scan available for the slave
    """
    return _get_ip_to_scan(slave_id=slave_id)


@exception_catcher
def _get_ip_to_scan(slave_id):
    scan = _get_scan_for_slave(slave_id)
    nmap_params = _parse_flags(arguments.flags)
    if scan:
        logger.info("Sending scan_{} of the ip: {} to slave {} whose ip is: {}".format(scan.id, scan.ip, slave_id,
                                                                                       request.remote_addr))
        return jsonify(
            contains_scan=True,
            scan_id=scan.id,
            conf={
                'params': arguments.flags,
                'ports': arguments.ports,
                'opt': nmap_params.nmap_params,
                'additional_params': nmap_params.additional_params
            }
        )
    else:
        logger.info("Slave {} whose ip is {} asked for a scan however there are no scans left :(".format(slave_id,
                                                                                                         request.remote_addr))
        return jsonify(
            contains_scan=False,
            message="There are no scans left for you :(",
        )


@app.route('/report_scan_results', methods=["POST"])
def report_scan_results():
    """
    An end point to which the slave reports once it completed on the scan or failed in the process.
    Expected Post parameters:
    :str slave_id: The id of the slave which handled the scan
    :str status: The status of the scan
    :int scan_id: The id of the scan
    :return:
    """
    post_params = request.json
    return _report_scan_results(slave_id=post_params.get('slave_id'),
                                scan_id=post_params.get('scan_id'),
                                status=post_params.get('status'))


@exception_catcher
def _report_scan_results(slave_id, scan_id, status):
    assert slave_id and scan_id and status, "Called _report_scan_results with the following params {} {} {}".format(
        slave_id, scan_id, status)
    scan = list(filter(lambda scan: scan.id == scan_id, scans))[0]
    logger.info(
        "Slave {} with ip: {} has completed scan_{} on ip: {} with the status: {}".format(
            slave_id,
            request.remote_addr,
            scan.id,
            scan.ip,
            status))
    scan.status = status
    if status == FAILED_STATUS:
        _handle_failed_scan(scan, slave_id)
    session.commit()
    # If all scans are done send an email
    if all([scan.status in (FAILED_STATUS, DONE_STATUS) for scan in scans]):
        _send_mail()
        logger.info("Finished the run :)")
    return "we don't care about the return value from this route"


def _handle_failed_scan(scan, slave_id):
    """
    Handle failed scan (choose whether to return the scan to the pool or mark it as failed)
    :param scan: The scan orm instance
    :param slave_id: The id of the slave which handles the scan
    """
    scan.failed_runs += 1
    scan.failed_slaves = ",".join(scan.failed_slaves.split(',') + [slave_id])
    if scan.failed_runs >= MAX_SCAN_RETRIES:
        logger.info(
            "Scan_{} has failed {} which is the maximum failed attempts allowed for a scan and therefore the scan"
            " is being forfeited")
        scan.status = FAILED_STATUS
    else:
        logger.info("Returning scan_{} to waiting pool.".format(scan.id))
        scan.status = WAITING_STATUS


def _get_scan_for_slave(slave_id):
    """
    Return a scan which the given slave can execute,
    In case a slave failed at running a scan it won't be able to execute it again
    :param slave_id: The id of the slave asking for the scan
    :return Scan: The scan
    """
    scans_that_are_waiting = list(
        filter(lambda scan: scan.status == WAITING_STATUS and slave_id not in scan.failed_slaves.split(','), scans))
    if scans_that_are_waiting:
        chosen_scan = scans_that_are_waiting[0]
        chosen_scan.status = SCANNING_STATUS
        session.commit()
        return chosen_scan


def _divide_range_to_single_ips(ip_range):
    """
    Divide a range of ips into a list of single ips.
    :param str ip_range: A range of ips for example 173.193.189.144/28
    :return:
    """
    return [str(single_ip) for single_ip in list(IPNetwork(ip_range))]


def _retrieve_ips_to_scan(source_file_name, divide_ips=False):
    """
    Retrieve a list of ips to scan.
    The function reads the list from a file.
    :param str source_file_name: The name of the file which contains the ips
    :param bool divide_ips: A flag which indicates whether an ip range should be divided to single ips
    :return list of str: A list of ips or ip ranges
    """
    with open(source_file_name) as ips_file:
        ip_ranges = [ip_range.strip() for ip_range in ips_file.readlines()]
        if divide_ips:
            return [ip
                    for ip_range in ip_ranges
                    for ip in _divide_range_to_single_ips(ip_range)]
        return ip_ranges


def _parse_flags(flags):
    """
    Parse the falgs that are passed to the script.
    This logic is taken from the main of seeker.py however the python code was altered a little bit (with no change
    to the logic)
    :param str flags: All of the flags that were passed to the script.
    :return tuple: (list of nmap arguments, additional arguments that will be passed to nmap on top of the default ones)
    """
    nmap_args = []
    additional_params = ''
    # Check if -Pn option is needed
    if 't' in flags:
        # Forced scan (skip discovery) on TCP Syn and UPD
        if 'v' in flags:
            if 't1' in flags:
                nmap_args.extend(['-sU -sV', '-sV', '-Pn -sU -sV', '-Pn -sV'])
            else:
                nmap_args.extend(['-sU -sV -Pn', '-sV -Pn'])
        else:
            if 't1' in flags:
                nmap_args.extend(['-sS', '-sU', '-Pn -sU', '-Pn -sV'])
            else:
                nmap_args.extend(['-Pn -sS', '-Pn -sU'])
    else:
        # Normal Scan
        if 'v' in flags:
            nmap_args.extend(['-sV', '-sU -A'])
        else:
            nmap_args.extend(['-sS', '-sU'])

    # Check Parameters------------------------------------
    # Protocol Scan. This triggers if option p is enabled
    if 'p' in flags:
        # Protocol Scanning
        nmap_args.append('-sO')

    # Include connect scan -sT
    if 'o' in flags:
        nmap_args.append('-sT')

    # Append Null and Fin on the scan
    nmap_args.extend(['-sN', '-sF'])

    # Include closed port
    if 'c' in flags:
        additional_params = ' -ddd'
    return NmapParameters(nmap_params=nmap_args, additional_params=additional_params)


def _create_new_scan(ip):
    """
    Create a new scan in db and return it
    :param str ip: The ip on which the scan will run
    """
    scan = NmapScan(ip=ip, status="In Queue")
    scan.status = WAITING_STATUS
    session.add(scan)
    session.commit()
    return scan


def _send_mail():
    """
    Sends an email notifying that the scan was complete
    """
    logger.info("Sending notification mail...")
    to = RECEIVER_MAIL
    subject = 'A Scan has finished!'
    text = 'The scans of the following ips: {} has finished'.format([scan.ip for scan in scans])

    server = smtplib.SMTP(SMTP_CONFIG['server'], SMTP_CONFIG['port'])
    server.ehlo()
    server.starttls()
    server.login(SENDER_MAIL, SENDER_PASSWORD)

    body = 'To: {}\r\nFROM: {}\r\nSUBJECT: {}\r\n\r\n{}'.format(to, SENDER_MAIL, subject, text)

    server.sendmail(SENDER_MAIL, [to], body)
    logger.info("Done sending email!")

    server.quit()


def parse_arguments():
    """
    Retrieves the arguments from the command line.
    :return int: The port in which the slave will listen.
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--file", dest='ips_source_file_name', type=str,
                        help='The name of the file which will '
                             'contain the list of ips (can be relative or absolute path)', required=True)
    parser.add_argument("--divide", dest='divide_ips', action='store_true',
                        help='A flag which indicates whether an ips range should be a single job for one'
                             'slave or should the master divide the range to single ips and each ip will'
                             'be considered a job for a slave', default=False)
    parser.add_argument("--flags", dest='flags', type=str,
                        help='The flags of the script (see the README.md file)', required=True)

    parser.add_argument("-p", "--ports", dest='ports', type=str,
                        help='The ports which will be scanned for the ips.', required=True)

    return parser.parse_args()


if __name__ == '__main__':
    arguments = parse_arguments()
    ips_to_scan = _retrieve_ips_to_scan(arguments.ips_source_file_name, divide_ips=arguments.divide_ips)
    scans = [_create_new_scan(ip) for ip in ips_to_scan]
    logger.info("The following ips will be scanned:")
    logger.info(", ".join([scan.ip for scan in scans]))
    app.run(host='0.0.0.0', port=REST_SERVER_PORT, debug=FLASK_DEBUG, use_reloader=False)
