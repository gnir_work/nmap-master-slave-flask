import argparse
import sys
from datetime import datetime as dt
from logbook import StreamHandler, FileHandler, Logger
from custom_exceptions import NmapScanException, DbException
from seeker import scan_port
from consts import REST_SERVER_PORT, REST_SERVER_IP, FAILED_STATUS, DONE_STATUS, SLAVE_DELAY_TIME
from writer import MysqlWriter
from db import get_session, get_scan_by_id
from multiprocessing import Process
import os
import requests
import time
from uuid import uuid4

SLAVE_LOG_FILE_FORMAT = 'slave_{id}.log'
DEFAULT_SLAVE_ID = uuid4().hex
port_add_arguments = ' -n --max-retries 2 --max-rtt-timeout 800ms --min-hostgroup 256 --min_parallelism 50 --max_parallelism 700'


def execute_scan(scan_id, conf):
    """
    Run the whole process of the scan
    :param scan_id: The id of the scan in the db
    :param conf: The configuration for the scan
    :return:
    """
    session = get_session()
    try:
        scan = get_scan_by_id(scan_id, session)
        logger.info("Killing all previous nmap processes")
        os.system('killall nmap')
        logger.info('working on {}'.format(scan.ip))
        scan.start_time = dt.now()
        session.commit()
        _run_scans_async(scan, conf)
    except NmapScanException:
        logger.exception()
        report_scan_results(scan_id=scan_id, status=FAILED_STATUS)
    except Exception:
        logger.exception()
        report_scan_results(scan_id, status=FAILED_STATUS)
        raise
    else:
        report_scan_results(scan_id=scan.id, status=DONE_STATUS)


def start():
    """
    Starts the slave.
    The slave will try requesting a ip to scan from the server, if the server doesnt have an ip the slave will try again
    after 2 seconds
    """
    while True:
        logger.info("Asking server for scan")
        response = _get_scan_from_server()
        # Raise any http exceptions
        response.raise_for_status()
        data = response.json()
        if data and data['contains_scan']:
            execute_scan(data['scan_id'], data['conf'])
        else:
            logger.info(data['message'])
            time.sleep(SLAVE_DELAY_TIME)
            print("Sleeping!!!")


def _get_scan_from_server():
    response = None
    while not response:
        try:
            response = requests.get(
                'http://{ip}:{port}/get_ip_to_scan/{slave_id}'.format(ip=REST_SERVER_IP, port=REST_SERVER_PORT,
                                                                      slave_id=SLAVE_ID))
        except Exception:
            logger.info("Server is down, waiting {} seconds till next try".format(SLAVE_DELAY_TIME))
            time.sleep(SLAVE_DELAY_TIME)
    return response


def report_scan_results(scan_id, status):
    requests.post("http://{ip}:{port}/report_scan_results".format(ip=REST_SERVER_IP, port=REST_SERVER_PORT),
                  json={'status': status, 'scan_id': scan_id, 'slave_id': SLAVE_ID})


def _run_scans_async(scan, conf):
    """
    Runs all of the scans that were issued in data async and wait for all of them to finish.
    :param dict data: The data passed from the master
    """
    ignore_closed_ports = 'n' in conf.get('params', '')
    logger.info("Running the following scans: {}".format(conf['opt']))
    processes = []
    for opt in conf['opt']:
        logger.info("Executing the folling scan:")
        logger.info("scan id: {} ignoring_no_response: {} nmap params: {}, ports: {}, script params: {}".format(
            scan.id, ignore_closed_ports, opt + port_add_arguments + conf['additional_params'], conf['ports'],
            conf['params']
        ))
        writer = MysqlWriter(npm_scan_id=scan.id, logger=logger,
                             ignore_closed_ports=ignore_closed_ports,
                             session=get_session(),
                             )
        processes.append(Process(target=run_scan, args=(opt, scan.ip, conf['ports'], conf['params'],
                                                        port_add_arguments + conf['additional_params'],
                                                        writer.write_results_to_db)))
    for processes in processes:
        processes.start()
        if processes.is_alive():
            processes.join()
        if processes.exitcode != 0:
            raise NmapScanException


def run_scan(opt, ip, ports, params, port_add_arguments, callback):
    scan_port(opt, ip=ip, ports=ports, params=params, port_add_arguments=port_add_arguments, callback=callback)


def get_slave_id():
    """
    Get the slave id that will be used in this run.
    :return str: The id of the slave
    """
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--id", dest='slave_id', type=str,
                        help='The id of the slave')

    return parser.parse_args().slave_id or DEFAULT_SLAVE_ID


if __name__ == '__main__':
    SLAVE_ID = get_slave_id()
    StreamHandler(sys.stdout, bubble=True, level='DEBUG').push_application()
    FileHandler(SLAVE_LOG_FILE_FORMAT.format(id=SLAVE_ID), bubble=True, level='INFO').push_application()
    logger = Logger('slave {}'.format(SLAVE_ID))
    try:
        start()
    except Exception:
        logger.exception()
        raise
