# README #
### What is this repository for? ###

This is a basic master-slave design pattern for running multiple nmap scans.
### Design:
There is a master which holds a list of ips to scan.
The master is a `rest API` server which has two views:
```
http://server_ip:server_port/get_ip_to_scan/<string:slave_id>
```
This is an `GET` view.
From this view you can recieve an ip from the list of ips
As you can see in the url this view expects one get parameter: `slave_id` of type `string`
```
http://server_ip:server_port/report_scan_results
```
This is a POST view, to this view the slaves post the status of the scan on complition or failure.
The view excpects the following post parameters:

* `slave_id`: The id of the slave which handled the scan (type `string`)
* `status`: The status of the scan (type `string`)
* `scan_id:` The id of the scan (type `int`)

At the end of all of the scans the master will send a mail to the defined emails (in `consts.py`)

## Configuration
Most of the script configuration is located in `consts.py` and are quite straightforward.
For example:
* The host of the sql db
* The ip and port of the rest api hosted on the master
* The email account information from which the emails will be sent
* The logging (The logging is located in the `master.py` and `slave.py` files)

## Parameters that can be passed to master (through --flags, see explanataion on running the master)
* The default is running the following 4 scans: -sS, -sU, -sN -sF
* `p` parameter will add another scan: -sO
* `t1` parameter will change -sS to -sS-Pn -sU to -sU-Pn using a predifined list of known vpn ports 
* `t2` parameter will change -sS to -sS-Pn -sU to -sU-Pn using the ports you specified
* `o` parameter will add anohter scanL -sT
* `v` includes service version: -sV
* `c` This will save to db both closed and open ports (By default its only open)
* `n` this will filter and exlude those ports with "no-response" status

## How do I get set up?
* pip install -r requirments.txt
* sudo apt-get install nmap

The slaves and master assume that there is a mysql server running in which they will save the scan results.
The configurations of db are in `consts.py` (user, password, server host, etc..)


## Setting up the mysql server
__For a detailed guide go to__: https://www.copahost.com/blog/grant-mysql-remote-access/

Step for setting up mysql server:

* First you need to edit the configuration file and replace the line `bind-address  = 127.0.0.1` to `bind-address   = 0.0.0.0`
    1. on debian 9 the file is located at: /etc/mysql/mysql.conf.d/mysqld.cnf
    2. on centos7.x by default the file is located at: /etc/my.cnf
    3. _note_: on centos7.x you can run the command in order to find the conf file: `/usr/libexec/mysqld --help --verbose` which will output a lot of text, look for the line: `Default options are read from the following files in the given order:`

* Restart the mysql server:
    + On debian 9 and centos7.x: 
        1. `systemctl stop mysql` 
        2. `systemctl start mysql`

* Setup a user for remote connection:
    1. connect to the mysql server by running `mysql -u root -p`, afterwards you will be prompted for the root password you chose on setup.
    2. Run the following command (This will create a user with root privileges): 
    ```mysql
    GRANT ALL PRIVILEGES ON *.* TO '[user-name]'@'%'      
    IDENTIFIED BY '[new-password]';
    FLUSH PRIVILEGES;
    ```

## Running the slaves
Running the slaves is as simeple as `python slave.py`,
*__However Please Note:__* The slaves need to be run in `sudo` for some of the scans, the slave will prompt you in case it isn't in sudo however needs to be.

## Running the master
When running the master you will be asked to pass several flags:

* `-f`, `--file` -> The name of the file which will contain the list of ips (can be relative or absolute path)
* `--divide` -> A flag which indicates whether an ips range should be a single job for one slave or should the master divide the range to single ips and each ip will be considered a job for a slave (default `True`)
* `--flags` -> The parameters that were explained the __parameters section__ above.
* `-p`, `--ports` -> The ports that will be scanned in the nmap scan.

## Examples
#### on first slave machine: 
```bash
python3 slave.py
```
#### on second slave machine:
```bash
python3 slave.py
```
#### on master: 
```bash
python3 master.py -f ips_to_scan.txt --flags pc --ports  3000-6000
```

#### ips_to_scan.txt:
```bash
127.0.0.1
159.122.141.152/29
```

This will start the master which will send each slave each time a different ip to scan (in a cycle).

__NOTE:__ if you will pass the `--divide` flag to the master than the ip `159.122.141.152/29` will be
divided to several ips (`159.122.141.152`, `159.122.141.153`, ..., `159.122.141.159`)

__NOTE:__ It doesn't matter if the master is run first or the slaves, and as a matter of fact you can leave the slaves on and run the master
as many times as you want with different ips and parameters. However it is recommended to start the master first.

## Error handling
In case a slave got an exception while handling an ip to scan it will report to master prior to exiting,
the master will than check if this scan has failed more than the allowed number of failures per task (configured in `consts.py`)
if that is indeed the case than the master will mark the scan as failed, otherwise it will update the scan which slave has failed 
executing it (so it won't be able to try again) and return the scans to the pool of scans that are still waiting.

__NOTE:__ The slave will die on every exception which isn't `NmapScanException`! (meaning any exception that isn't related to the nnmap script)